package storage

import (
	"fmt"

	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"
	gu_sql "gitlab.com/zestlabs-io/go-utils/sql"
)

type Storage struct {
	db *sqlx.DB
}

func New(address string) (*Storage, error) {
	db, err := sqlx.Open("pgx", address)
	if err != nil {
		return nil, err
	}

	return &Storage{db}, nil
}

func (p *Storage) MigrateDB() error {
	fmt.Println("Running database migrations...")

	dbVersioner, err := gu_sql.InitDBVersioner("job_listing", p.db)
	if err != nil {
		return err
	}

	dbVersioner.AddStatement("createJobListingsTable", `
	CREATE TABLE IF NOT EXISTS job_listings (
		id SERIAL PRIMARY KEY,
		position VARCHAR(255),
		company_id INTEGER,
		salary INTEGER,
		tech_stack VARCHAR[],
		being_advertised BOOLEAN);`)

	dbVersioner.AddStatement("createCompaniesable", `
	CREATE TABLE IF NOT EXISTS companies (
		id SERIAL PRIMARY KEY,
		name VARCHAR(255));`)

	if err := dbVersioner.CheckAndUpdateDB(); err != nil {
		return err
	}

	dbVersioner.Reset()
	return nil
}
