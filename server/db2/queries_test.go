package storage

import (
	"context"
	"testing"

	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
)

func TestGetJobsByCompanyId(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	var jobsTest []JobListingDB
	jobsTest, err = testDB.GetJobsByCompanyId(ctx, int32(1))
	assert.Nil(t, err)
	assert.Equal(t, jobsTest[0], jobs[0])
	assert.Equal(t, jobsTest[1], jobs[2])
	assert.Len(t, jobsTest, 2)
	t.Cleanup(func() {
		err2 := Cleanup(ctx)
		assert.Nil(t, err2)
	})
}

func TestGetAllCompanies(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	var res []Company
	err = testDB.db.SelectContext(ctx, &res, sqlSelectCompanyWhere)
	assert.Nil(t, err)
	assert.Equal(t, res[0], *company)
	assert.Len(t, res, 1)
	t.Cleanup(func() {
		err2 := Cleanup(ctx)
		assert.Nil(t, err2)
	})
}

func TestGetCompanyByName(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	var res Company
	err = testDB.db.GetContext(ctx, &res, sqlSelectCompanyByName, "Tester1")
	assert.Nil(t, err)
	assert.Equal(t, *company, res)
	t.Cleanup(func() {
		err2 := Cleanup(ctx)
		assert.Nil(t, err2)
	})
}

func TestAddCompany(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	t.Cleanup(func() {
		err2 := Cleanup(ctx)
		assert.Nil(t, err2)
	})
}

func TestGetJobsByStack(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	stack := []string{"Go", "Docker"}
	var res []JobListingDB
	err = testDB.db.SelectContext(ctx, &res, sqlSelectJobListingByStack, stack)
	assert.Nil(t, err)
	assert.Equal(t, jobs[0], res[0])
	assert.Equal(t, jobs[2], res[1])
	assert.Len(t, res, 2)
	t.Cleanup(func() {
		err2 := Cleanup(ctx)
		assert.Nil(t, err2)
	})
}

func TestGetAllJobs(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	var res []JobListingDB
	res, err = testDB.GetAllJobs(ctx)
	assert.Nil(t, err)
	assert.Equal(t, jobs, res)
	assert.Len(t, res, 3)
	t.Cleanup(func() {
		err2 := Cleanup(ctx)
		assert.Nil(t, err2)
	})
}

func TestDeleteJobById(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	t.Cleanup(func() {
		err2 := Cleanup(ctx)
		assert.Nil(t, err2)
	})
}

func TestDeleteCompanyById(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	t.Cleanup(func() {
		err2 := Cleanup(ctx)
		assert.Nil(t, err2)
	})
}

func TestGetJobById(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	var res *JobListingDB
	res, err = testDB.GetJobById(ctx, int32(1))
	assert.Nil(t, err)
	assert.Equal(t, jobs[0], *res)
	t.Cleanup(func() {
		err2 := Cleanup(ctx)
		assert.Nil(t, err2)
	})
}

func TestUpdateJobById(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	err = testDB.UpdateJobById(ctx, int32(1), jobs[1])
	assert.Nil(t, err)
	t.Cleanup(func() {
		err2 := Cleanup(ctx)
		assert.Nil(t, err2)
	})
}

func TestAddJob(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	t.Cleanup(func() {
		err2 := Cleanup(ctx)
		assert.Nil(t, err2)
	})
}

func TestAdvertiseJob(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	err = testDB.AdvertiseJob(ctx, int32(1))
	assert.Nil(t, err)
	t.Cleanup(func() {
		err2 := Cleanup(ctx)
		assert.Nil(t, err2)
	})
}

func SetUpMocks() (*Company, []JobListingDB) {
	company := Company{
		Name: "Tester1",
	}

	job1 := JobListingDB{
		Position:        "Software Engineer",
		CompanyId:       1,
		Salary:          8000,
		TechStack:       pq.StringArray{"Go", "JavaScript", "Docker"},
		BeingAdvertised: true,
	}

	job2 := JobListingDB{
		Position:        "Data Scientist",
		CompanyId:       2,
		Salary:          5000,
		TechStack:       pq.StringArray{"Python", "SQL", "Machine Learning"},
		BeingAdvertised: false,
	}

	job3 := JobListingDB{
		Position:        "DevOps Engineer",
		CompanyId:       1,
		Salary:          3500,
		TechStack:       pq.StringArray{"Docker", "Kubernetes", "Jenkins"},
		BeingAdvertised: true,
	}

	jobs := make([]JobListingDB, 0)

	jobs = append(jobs, job1, job2, job3)

	return &company, jobs
}

func SetUp(ctx context.Context, company *Company, jobs []JobListingDB) error {
	resetSequenceQuery := "ALTER SEQUENCE companies_id_seq RESTART WITH 1;"
	_, err := testDB.db.Exec(resetSequenceQuery)
	if err != nil {
		return err
	}

	resetSequenceQuery = "ALTER SEQUENCE job_listings_id_seq RESTART WITH 1;"
	_, err = testDB.db.Exec(resetSequenceQuery)
	if err != nil {
		return err
	}
	_, err = testDB.db.Exec("DELETE FROM job_listings")
	if err != nil {
		return err
	}

	_, err = testDB.db.Exec("DELETE FROM companies")
	if err != nil {
		return err
	}

	if err := testDB.AddCompany(ctx, company); err != nil {
		return err
	}

	for _, job := range jobs {
		if err := testDB.AddJob(ctx, &job); err != nil {
			return err
		}
	}
	jobs[0].ID = 1
	jobs[1].ID = 2
	jobs[2].ID = 3
	company.ID = 1
	return nil
}

func Cleanup(ctx context.Context) error {
	_, err := testDB.db.Exec("DELETE FROM job_listings")
	if err != nil {
		return err
	}

	_, err = testDB.db.Exec("DELETE FROM companies")
	if err != nil {
		return err
	}
	return nil
}
