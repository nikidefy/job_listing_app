package storage

import (
	"testing"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

func TestVersioner(t *testing.T) {
	sqlTableExists := `SELECT EXISTS (SELECT FROM pg_tables WHERE tablename = $1);`

	jobListingsExist, err := tableExists(testDB.db, sqlTableExists, "job_listings")
	assert.Nil(t, err)
	assert.True(t, jobListingsExist)

	companiesExist, err := tableExists(testDB.db, sqlTableExists, "companies")
	assert.Nil(t, err)
	assert.True(t, companiesExist)
}

func tableExists(db *sqlx.DB, query string, tableName string) (bool, error) {
	var exists bool
	err := db.Get(&exists, query, tableName)
	return exists, err
}
