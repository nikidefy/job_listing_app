package storage

import "github.com/lib/pq"

type JobListingDB struct {
	ID              int32          `db:"id" dbgen:"noupdate,serial"`
	Position        string         `db:"position"`
	CompanyId       int32          `db:"company_id"`
	Salary          int32          `db:"salary"`
	TechStack       pq.StringArray `db:"tech_stack"`
	BeingAdvertised bool           `db:"being_advertised"`
}

type Company struct {
	ID   int32  `db:"id" dbgen:"noupdate,serial"`
	Name string `db:"name"`
}
