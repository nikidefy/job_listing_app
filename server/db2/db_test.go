package storage

import (
	"log"
	"os"
	"testing"
)

var testDB *Storage

func TestMain(m *testing.M) {
	os.Setenv("DB_URL_TEST", "postgres://postgres:Nikideni5588@localhost/jobsTest?sslmode=disable")

	var err error
	testDB, err = New(os.Getenv("DB_URL_TEST"))
	if err != nil {
		log.Fatal("Couldn't connect to db test", err)
	}

	if err = testDB.MigrateDB(); err != nil {
		log.Fatal("Couldn't migrate db test", err)
	}

	os.Exit(m.Run())
}
