package storage

import (
	"context"
	"fmt"
)

func (s *Storage) GetJobsByCompanyId(ctx context.Context, companyId int32) ([]JobListingDB, error) {
	var jobs []JobListingDB
	if err := s.db.SelectContext(ctx, &jobs, sqlSelectJobListingByCompanyId, companyId); err != nil {
		fmt.Println("Couldnt get jobs by company id", err)
		return nil, err
	}
	return jobs, nil
}

func (s *Storage) GetAllCompanies(ctx context.Context) ([]Company, error) {
	var companies []Company
	if err := s.db.SelectContext(ctx, &companies, sqlSelectCompanyWhere); err != nil {
		fmt.Println("Couldnt get all companies", err)
		return nil, err
	}
	return companies, nil
}

func (s *Storage) GetCompanyByName(ctx context.Context, name string) (*Company, error) {
	var company Company
	if err := s.db.GetContext(ctx, &company, sqlSelectCompanyByName, name); err != nil {
		fmt.Println("Couldnt get company by name", err)
		return &company, err
	}
	return &company, nil
}

func (s *Storage) AddCompany(ctx context.Context, company *Company) error {
	if _, err := s.db.NamedExecContext(ctx, sqlInsertCompany, company); err != nil {
		fmt.Println("Couldnt register company", err)
		return err
	}
	return nil
}

func (s *Storage) GetJobsByStack(ctx context.Context, stack []string) ([]JobListingDB, error) {
	var jobs []JobListingDB
	if err := s.db.SelectContext(ctx, &jobs, sqlSelectJobListingByStack, stack); err != nil {
		fmt.Println("Couldnt get jobs by tech stack", err)
		return nil, err
	}
	return jobs, nil
}

func (s *Storage) GetAllJobs(ctx context.Context) ([]JobListingDB, error) {
	var jobs []JobListingDB
	if err := s.db.SelectContext(ctx, &jobs, sqlSelectJobListingDBWhere); err != nil {
		fmt.Println("Couldnt get all jobs", err)
		return nil, err
	}
	return jobs, nil
}

func (s *Storage) DeleteJobById(ctx context.Context, jobId int32) error {
	if _, err := s.db.ExecContext(ctx, sqlDeleteJobListingDB, jobId); err != nil {
		fmt.Println("Couldn't delete job")
		return err
	}
	return nil
}

func (s *Storage) DeleteCompanyById(ctx context.Context, companyId int32) error {
	if _, err := s.db.ExecContext(ctx, sqlDeleteCompany, companyId); err != nil {
		fmt.Println("Couldn't delete company")
		return err
	}
	return nil
}

func (s *Storage) GetJobById(ctx context.Context, jobId int32) (*JobListingDB, error) {
	var job JobListingDB
	if err := s.db.GetContext(ctx, &job, sqlSelectJobListingDBByID, jobId); err != nil {
		fmt.Println("Couldnt get listing by id", err)
		return &job, err
	}
	return &job, nil
}

func (s *Storage) UpdateJobById(ctx context.Context, jobId int32, job JobListingDB) error {
	_, err := s.db.ExecContext(ctx, sqlUpdateJobListingDB, job.Position, job.CompanyId, job.Salary, job.TechStack, job.BeingAdvertised, jobId)
	if err != nil {
		fmt.Println("Couldn't update job")
		return err
	}
	return nil
}

func (s *Storage) AddJob(ctx context.Context, job *JobListingDB) error {
	if _, err := s.db.NamedExecContext(ctx, sqlInsertJobListingDB, job); err != nil {
		fmt.Println("Couldnt add job", err)
		return err
	}

	return nil
}

func (s *Storage) AdvertiseJob(ctx context.Context, id int32) error {
	if _, err := s.db.ExecContext(ctx, sqlUpdateJobListingDBAds, id); err != nil {
		fmt.Println("Couldnt add job", err)
		return err
	}

	return nil
}

func (s *Storage) ResetId(ctx context.Context) error {
	resetSequenceQuery := "ALTER SEQUENCE companies_id_seq RESTART WITH 1;"
	_, err := s.db.Exec(resetSequenceQuery)
	if err != nil {
		return err
	}

	resetSequenceQuery = "ALTER SEQUENCE job_listings_id_seq RESTART WITH 1;"
	_, err = s.db.Exec(resetSequenceQuery)
	if err != nil {
		return err
	}
	_, err = s.db.Exec("DELETE FROM job_listings")
	if err != nil {
		return err
	}

	_, err = s.db.Exec("DELETE FROM companies")
	if err != nil {
		return err
	}
	return nil
}
