package openapi

import (
	db "db2"
	"log"
	"os"
	"testing"
)

var testDefaultAPIService *DefaultAPIService
var testDB *db.Storage

func TestMain(m *testing.M) {
	log.Printf("Server started")
	os.Setenv("DB_URL", "postgres://postgres:Nikideni5588@localhost/jobsTest?sslmode=disable")
	dbUrl := os.Getenv("DB_URL")
	if dbUrl == "" {
		log.Fatal("DB_URl not found in env")
	}

	var err error
	testDB, err = db.New(dbUrl)
	if err != nil {
		log.Fatal("Couldn't connect to db", err)
	}

	if err = testDB.MigrateDB(); err != nil {
		log.Fatal("Couldn't migrate db", err)
	}

	servicer := NewDefaultAPIService(testDB)
	var ok bool
	if testDefaultAPIService, ok = servicer.(*DefaultAPIService); !ok {
		log.Fatal("Couldn't create service", err)
	}
	os.Exit(m.Run())
}
