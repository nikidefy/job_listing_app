/*
 * Combined API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package openapi




type Company struct {

	CompanyID int32 `json:"companyID,omitempty"`

	Name string `json:"Name,omitempty"`
}

// AssertCompanyRequired checks if the required fields are not zero-ed
func AssertCompanyRequired(obj Company) error {
	return nil
}

// AssertCompanyConstraints checks if the values respects the defined constraints
func AssertCompanyConstraints(obj Company) error {
	return nil
}
