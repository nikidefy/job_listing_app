/*
 * Combined API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package openapi

import (
	"context"
	db "db2"
	"fmt"
)

// DefaultAPIService is a service that implements the logic for the DefaultAPIServicer
// This service should implement the business logic for every endpoint for the DefaultAPI API.
// Include any external packages or services that will be required by this service.
type DefaultAPIService struct {
	s *db.Storage
}

// NewDefaultAPIService creates a default api service
func NewDefaultAPIService(s *db.Storage) DefaultAPIServicer {
	return &DefaultAPIService{s}
}

// CompanyCompanyIdJobsGet - Get jobs by company ID
func (s *DefaultAPIService) CompanyCompanyIdJobsGet(ctx context.Context, companyId int32) (ImplResponse, error) {
	jobs, err := s.s.GetJobsByCompanyId(ctx, companyId)
	if err != nil {
		fmt.Println("Couldn't get all listings by company id")
		return Response(500, nil), err
	}
	return Response(200, jobs), nil
}

// CompanyGet - Get all companies
func (s *DefaultAPIService) CompanyGet(ctx context.Context) (ImplResponse, error) {
	companies, err := s.s.GetAllCompanies(ctx)
	if err != nil {
		fmt.Println("Couldn't get all companies")
		return Response(500, nil), err
	}
	return Response(200, companies), nil
}

// CompanyNameGet - Get company by name
func (s *DefaultAPIService) CompanyNameGet(ctx context.Context, name string) (ImplResponse, error) {
	company, err := s.s.GetCompanyByName(ctx, name)
	if err != nil {
		fmt.Println("Couldn't get company by name")
		return Response(500, nil), err
	}
	return Response(200, company), nil
}

// CompanyPost - Create a new company
func (s *DefaultAPIService) CompanyPost(ctx context.Context, companyN CompanyN) (ImplResponse, error) {
	company := db.Company{
		Name: companyN.NameN,
	}
	if err := s.s.AddCompany(ctx, &company); err != nil {
		fmt.Println("Couldn't register company")
		return Response(500, nil), err
	}
	return Response(201, nil), nil
}

// JobGet - Get all job listings
func (s *DefaultAPIService) JobGet(ctx context.Context) (ImplResponse, error) {
	jobs, err := s.s.GetAllJobs(ctx)
	if err != nil {
		fmt.Println("Couldn't get all jobs")
		return Response(500, nil), err
	}
	return Response(200, jobs), nil
}

// JobIdDelete - Delete job listing by ID
func (s *DefaultAPIService) JobIdDelete(ctx context.Context, id int32) (ImplResponse, error) {
	if err := s.s.DeleteJobById(ctx, id); err != nil {
		fmt.Println("Couldn't delete job by id")
		return Response(500, nil), err
	}
	return Response(201, nil), nil
}

// JobIdGet - Get job listing by ID
func (s *DefaultAPIService) JobIdGet(ctx context.Context, id int32) (ImplResponse, error) {
	job, err := s.s.GetJobById(ctx, id)
	if err != nil {
		fmt.Println("Couldn't get jobs by id")
		return Response(500, nil), err
	}
	return Response(200, job), nil
}

// JobIdPut - Update job listing by ID
func (s *DefaultAPIService) JobIdPut(ctx context.Context, id int32, jobListing JobListing) (ImplResponse, error) {
	jobN := db.JobListingDB{
		Position:        jobListing.Position,
		CompanyId:       jobListing.CompanyId,
		Salary:          jobListing.Salary,
		TechStack:       jobListing.TechStack,
		BeingAdvertised: jobListing.BeingAdvertised,
	}
	if err := s.s.UpdateJobById(ctx, id, jobN); err != nil {
		fmt.Println("Couldn't update job by id")
		return Response(500, nil), err
	}
	return Response(201, nil), nil
}

// JobPost - Create a new job listing
func (s *DefaultAPIService) JobPost(ctx context.Context, jobListingN JobListingN) (ImplResponse, error) {
	jobN := db.JobListingDB{
		Position:        jobListingN.PositionN,
		CompanyId:       jobListingN.CompanyIdN,
		Salary:          jobListingN.SalaryN,
		TechStack:       jobListingN.TechStackN,
		BeingAdvertised: jobListingN.BeingAdvertisedN,
	}
	if err := s.s.AddJob(ctx, &jobN); err != nil {
		fmt.Println("Couldn't update job by id")
		return Response(500, nil), err
	}
	return Response(201, nil), nil
}

// JobStartAddIdPost - Start add for job offer
func (s *DefaultAPIService) JobStartAddIdPost(ctx context.Context, id int32) (ImplResponse, error) {
	if err := s.s.AdvertiseJob(ctx, id); err != nil {
		fmt.Println("Couldn't start or stop advertising")
		return Response(500, nil), err
	}
	return Response(200, nil), nil
}

// JobTechstackTechstackGet - Get job listings by tech stack
func (s *DefaultAPIService) JobTechstackTechstackGet(ctx context.Context, techstack []string) (ImplResponse, error) {
	jobs, err := s.s.GetJobsByStack(ctx, techstack)
	if err != nil {
		fmt.Println("Couldn't get jobs by tech stack")
		return Response(500, nil), err
	}
	return Response(200, jobs), nil
}

// CompanyDelete - Delete company by Id
func (s *DefaultAPIService) CompanyDelete(ctx context.Context, id int32) (ImplResponse, error) {
	if err := s.s.DeleteCompanyById(ctx, id); err != nil {
		fmt.Println("Couldn't delete company by id")
		return Response(500, nil), err
	}
	return Response(201, nil), nil
}

func (s *DefaultAPIService) Reset(ctx context.Context) (ImplResponse, error) {
	if err := s.s.ResetId(ctx); err != nil {
		fmt.Println("Couldnt reset")
		return Response(500, nil), err
	}
	return Response(201, nil), nil
}
