package openapi

import (
	"context"
	m "db2"
	"testing"

	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
)

func TestCompanyCompanyIdJobsGet(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)

	jobsRes := []m.JobListingDB{jobs[0], jobs[2]}
	response, err1 := testDefaultAPIService.CompanyCompanyIdJobsGet(ctx, company.ID)
	assert.Nil(t, err1)
	assert.NotNil(t, response)
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, jobsRes, response.Body)
	err = Cleanup(ctx)
	assert.Nil(t, err)
}

func TestCompanyGet(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	companies := []m.Company{*company}
	response, err1 := testDefaultAPIService.CompanyGet(ctx)
	assert.Nil(t, err1)
	assert.NotNil(t, response)
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, companies, response.Body)
	err = Cleanup(ctx)
	assert.Nil(t, err)
}

func TestCompanyNameGet(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	response, err1 := testDefaultAPIService.CompanyNameGet(ctx, "Tester1")
	assert.Nil(t, err1)
	assert.NotNil(t, response)
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, company, response.Body)
	err = Cleanup(ctx)
	assert.Nil(t, err)
}

func TestCompanyPost(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	err = Cleanup(ctx)
	assert.Nil(t, err)
}

func TestJobGet(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	response, err1 := testDefaultAPIService.JobGet(ctx)
	assert.Nil(t, err1)
	assert.NotNil(t, response)
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, jobs, response.Body)
	err = Cleanup(ctx)
	assert.Nil(t, err)
}

func TestJobIdDelete(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	response, err1 := testDefaultAPIService.JobIdDelete(ctx, int32(1))
	assert.Nil(t, err1)
	assert.NotNil(t, response)
	assert.Equal(t, 201, response.Code)
	err = Cleanup(ctx)
	assert.Nil(t, err)
}

func TestJobIdGet(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	response, err1 := testDefaultAPIService.JobIdGet(ctx, int32(1))
	assert.Nil(t, err1)
	assert.NotNil(t, response)
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, &jobs[0], response.Body)
	err = Cleanup(ctx)
	assert.Nil(t, err)
}

func TestJobIdPut(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	jobN2 := JobListing{
		JobID:           1,
		Position:        jobs[1].Position,
		CompanyId:       jobs[1].CompanyId,
		Salary:          jobs[1].Salary,
		TechStack:       jobs[1].TechStack,
		BeingAdvertised: jobs[1].BeingAdvertised,
	}
	response, err1 := testDefaultAPIService.JobIdPut(ctx, int32(1), jobN2)
	assert.Nil(t, err1)
	assert.NotNil(t, response)
	assert.Equal(t, 201, response.Code)
	err = Cleanup(ctx)
	assert.Nil(t, err)
}

func TestJobPost(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	err = Cleanup(ctx)
	assert.Nil(t, err)
}

func TestJobStartAddIdPost(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	response, err1 := testDefaultAPIService.JobStartAddIdPost(ctx, 1)
	assert.Nil(t, err1)
	assert.NotNil(t, response)
	assert.Equal(t, 200, response.Code)
	err = Cleanup(ctx)
	assert.Nil(t, err)
}

func TestJobTechstackTechstackGet(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	jobsRes := []m.JobListingDB{jobs[0], jobs[2]}
	response, err1 := testDefaultAPIService.JobTechstackTechstackGet(ctx, []string{"Go", "Docker"})
	assert.Nil(t, err1)
	assert.NotNil(t, response)
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, jobsRes, response.Body)
	err = Cleanup(ctx)
	assert.Nil(t, err)
}

func TestCompanyDelete(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	response, err1 := testDefaultAPIService.CompanyDelete(ctx, int32(1))
	assert.Nil(t, err1)
	assert.NotNil(t, response)
	assert.Equal(t, 201, response.Code)
	err = Cleanup(ctx)
	assert.Nil(t, err)
}

func TestReset(t *testing.T) {
	ctx := context.Background()
	company, jobs := SetUpMocks()
	err := SetUp(ctx, company, jobs)
	assert.Nil(t, err)
	response, err1 := testDefaultAPIService.Reset(ctx)
	assert.Nil(t, err1)
	assert.NotNil(t, response)
	assert.Equal(t, 201, response.Code)
	err = Cleanup(ctx)
	assert.Nil(t, err)
}

func SetUpMocks() (*m.Company, []m.JobListingDB) {
	company := m.Company{
		ID:   1,
		Name: "Tester1",
	}

	job1 := m.JobListingDB{
		ID:              1,
		Position:        "Software Engineer",
		CompanyId:       1,
		Salary:          8000,
		TechStack:       pq.StringArray{"Go", "JavaScript", "Docker"},
		BeingAdvertised: true,
	}

	job2 := m.JobListingDB{
		ID:              2,
		Position:        "Data Scientist",
		CompanyId:       2,
		Salary:          5000,
		TechStack:       pq.StringArray{"Python", "SQL", "Machine Learning"},
		BeingAdvertised: false,
	}

	job3 := m.JobListingDB{
		ID:              3,
		Position:        "DevOps Engineer",
		CompanyId:       1,
		Salary:          3500,
		TechStack:       pq.StringArray{"Docker", "Kubernetes", "Jenkins"},
		BeingAdvertised: true,
	}

	jobs := make([]m.JobListingDB, 0)

	jobs = append(jobs, job1, job2, job3)

	return &company, jobs
}

func SetUp(ctx context.Context, company *m.Company, jobs []m.JobListingDB) error {
	if err := Cleanup(ctx); err != nil {
		return err
	}

	companyN := CompanyN{
		NameN: company.Name,
	}
	response, err := testDefaultAPIService.CompanyPost(ctx, companyN)

	if response.Body != nil || response.Code != 201 || err != nil {
		return err
	}

	jobN1 := JobListingN{
		PositionN:        jobs[0].Position,
		CompanyIdN:       jobs[0].CompanyId,
		SalaryN:          jobs[0].Salary,
		TechStackN:       jobs[0].TechStack,
		BeingAdvertisedN: jobs[0].BeingAdvertised,
	}
	jobN2 := JobListingN{
		PositionN:        jobs[1].Position,
		CompanyIdN:       jobs[1].CompanyId,
		SalaryN:          jobs[1].Salary,
		TechStackN:       jobs[1].TechStack,
		BeingAdvertisedN: jobs[1].BeingAdvertised,
	}
	jobN3 := JobListingN{
		PositionN:        jobs[2].Position,
		CompanyIdN:       jobs[2].CompanyId,
		SalaryN:          jobs[2].Salary,
		TechStackN:       jobs[2].TechStack,
		BeingAdvertisedN: jobs[2].BeingAdvertised,
	}

	jobsN := make([]JobListingN, 0)
	jobsN = append(jobsN, jobN1, jobN2, jobN3)

	for _, job := range jobsN {
		response, err := testDefaultAPIService.JobPost(ctx, job)
		if response.Body != nil || response.Code != 201 || err != nil {
			return err
		}
	}
	return nil
}

func Cleanup(ctx context.Context) error {
	if _, err := testDefaultAPIService.Reset(ctx); err != nil {
		return err
	}
	return nil
}
