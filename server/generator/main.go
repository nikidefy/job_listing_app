package main

import (
	postgres "db2"
	"fmt"
	"os"

	gu_sql "gitlab.com/zestlabs-io/go-utils/sql"
)

const output = "db2/sql_statements.go"

func main() {
	fmt.Printf("Running SQL generation\n")

	gen, _ := gu_sql.NewSQLStatementGenerator("storage")
	gen.AddModelMeta("job_listing", postgres.JobListingDB{})
	gen.AddModelMeta("users", postgres.Company{})

	f, err := os.Create(output)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	if err := gen.GenerateStatements(f); err != nil {
		os.Exit(42)
	}

}
