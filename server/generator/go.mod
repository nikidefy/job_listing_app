module generator

go 1.21.4

replace gitlab.com/zestlabs-io/go-utils/sql => /home/nikolaigod/sql_needed/sql

require gitlab.com/zestlabs-io/go-utils/sql v0.0.0-00010101000000-000000000000

require (
	github.com/jmoiron/sqlx v1.3.5 // indirect
	github.com/lib/pq v1.10.9 // indirect
)
